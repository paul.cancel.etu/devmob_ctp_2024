package fr.iutlille.ctp2024;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import fr.iutlille.ctp2024.databinding.SuperTacheBinding;
import fr.iutlille.ctp2024.databinding.TachesBinding;

public class TacheAdapter extends RecyclerView.Adapter<TacheViewHolder> {
    private List<Tache> taches;

    public TacheAdapter(List<Tache> liste){
        this.taches = liste;
    }

    @NonNull
    @Override
    public TacheViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TachesBinding binding = TachesBinding.inflate(
            LayoutInflater.from(parent.getContext()),
            parent,
                false
        );
        return new TacheViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull TacheViewHolder holder, int position) {
        Tache tache = this.taches.get(position);
    }

    @Override
    public int getItemCount() {
        return this.taches.size();
    }
}
