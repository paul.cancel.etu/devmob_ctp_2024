package fr.iutlille.ctp2024;


import androidx.recyclerview.widget.RecyclerView;

import fr.iutlille.ctp2024.databinding.SuperTacheBinding;
import fr.iutlille.ctp2024.databinding.TachesBinding;

public class TacheViewHolder extends RecyclerView.ViewHolder{
    private final TachesBinding ui;

    public TacheViewHolder(TachesBinding ui){
        super(ui.getRoot());
        this.ui = ui;
    }

    public void setTache(Tache tache){
        ui.nom.setText(tache.getNom());
    }
}
