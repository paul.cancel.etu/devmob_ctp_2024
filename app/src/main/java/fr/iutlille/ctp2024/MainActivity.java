package fr.iutlille.ctp2024;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import fr.iutlille.ctp2024.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private ActivityMainBinding ui;
    private TacheAdapter adapter;
    private TacheApplication context;
    private List<Tache> liste;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ui = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(R.layout.super_tache);
        setTitle("Super Tâches");

        this.context = (TacheApplication) this.getApplication();
        this.liste = context.getTaches();
        // this.adapter = new TacheAdapter(liste);

        TextView tv = findViewById(R.id.textView2);
        tv.setText("A faire : " + this.toDo());

        tv = findViewById(R.id.textView3);
        tv.setText("Urgent : " + this.countUrgent());

        Button btn = (Button) findViewById(R.id.button2);
        btn.setOnClickListener(this);

        RecyclerView.LayoutManager lm = new LinearLayoutManager(this);
    }

    @Override
    public void onClick(View btn) {
        Intent intent;

        if(btn.getId() == R.id.button2){
            intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
        }
    }

    private int countUrgent(){
        int cpt = 0;
        for(Tache t : this.liste){
            if(t.getPriorite() == Priorite.HAUTE) cpt ++;
        }
        return cpt;
    }

    private int toDo(){
        int cpt = 0;
        for(Tache t : this.liste){
            if(t.getStatus() == Status.AFAIRE) cpt ++;
        }
        return cpt;
    }
}